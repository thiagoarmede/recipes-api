import {
    getIngredientById,
    getAllIngredients,
    createIngredient
} from "../db/models/ingredient";
import { User, createUser } from "../db/models/user";
import { Recipe, getRecipes, createRecipe } from "../db/models/recipe";

export const resolvers = {
    Query: {
        getAllIngredients: async () => await getAllIngredients(),
        getIngredient: async (parent: any, args: any) =>
            await getIngredientById(args as { id: string }),
        getRecipes: async (parent: any, {qtd}: any) =>
            await getRecipes(qtd),
    },

    Mutation: {
        createIngredient: async (parent: any, args: any) =>
            await createIngredient(args as {
                name: string;
                avatarUrl?: string;
            }),
        createRecipe: async (parent: any, args: any) => await createRecipe(args.input),
        createUser: async (parent: any, args: any) => await createUser(args as User)
    }
    
};
