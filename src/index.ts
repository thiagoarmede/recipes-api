import {GraphQLServer} from "graphql-yoga";
import { resolvers } from "./graphql/resolvers";
import { connectDatabase } from "./db/connection";

const server = new GraphQLServer({
    typeDefs: "./src/graphql/schema.graphql",
    resolvers,
});

server.start({port: 4000}).then(() => {
    console.log(`🚀 Server ready!`);
    connectDatabase();    
});
