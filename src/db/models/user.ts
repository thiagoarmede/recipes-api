import { model, Document} from 'mongoose';
import { UserSchema } from '../schemas/userSchema';

export interface User extends Document {
    id?: string;
    name: string;
    age?: number;
    avatarUrl?: string;
    nationality?: string;
}


const UserModel = model<User>('User', UserSchema, 'users');

export async function getUserById(id: string) {
    return await UserModel.findById(id).catch(err => {throw err});
}

export async function getAllUsers() {
    return await UserModel.find().catch(err => {throw err});
}

export async function createUser(user: User) {
    const newUser = new UserModel({
        ...user
    });

    return await newUser.save().catch(err => {throw err});
} 

