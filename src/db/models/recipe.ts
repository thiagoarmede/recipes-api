import { Document, model } from 'mongoose';
import {User} from './user';
import { Ingredient } from './ingredient';
import { RecipeSchema } from '../schemas/recipeSchema';
import { errorTap } from '../../utils/helpers';

export interface Recipe extends Document {
    id?: string;
    name: string;
    description?: string;
    cookTime: number;
    cookDescription: string;
    author: User;
    stars?: number;
    ingredients: Ingredient[];
}

const RecipeModel = model<Recipe>('Recipe', RecipeSchema, "recipes");

export async function getRecipeByAuthor(authorId: string) {
    return await RecipeModel.findOne({
        author: {
            authorId
        }
    });
}

export async function createRecipe(recipeInfo: Recipe) {
    const newRecipe = new RecipeModel({
        ...recipeInfo,
        stars: 0,
    });

    return await newRecipe.save().catch(errorTap(console.log));
}

export async function getRecipes(qtd = 5) {
    const recipes = await RecipeModel.find();
    return recipes.sort((a, b) => a.stars! - b.stars!).slice(qtd);     
}



