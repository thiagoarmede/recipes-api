import { model, Document } from "mongoose";
import { IngredientSchema } from '../schemas/ingredientSchema';
import { errorTap } from "../../utils/helpers";

export interface Ingredient extends Document {
    id?: string;
    name: string;
    avatarUrl?: string;
}

export const IngredientModel = model<Ingredient>(
    "Ingredient",
    IngredientSchema,
    "ingredients"
);

export async function getIngredientById(args: { id: string }) {
    const result = await IngredientModel.findById(args.id).catch(err => {
        throw err;
    });
    return result;
}

export async function getAllIngredients() {
    return await IngredientModel.find();
}

export const createIngredient = async (ingredientInfo: {
    name: string;
    avatarUrl?: string;
}) => {
    const newIngredient = new IngredientModel({
        name: ingredientInfo.name,
        avatarUrl: ingredientInfo.avatarUrl || null
    });

    return await newIngredient.save().catch(errorTap(console.log));
};
