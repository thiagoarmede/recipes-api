import mongoose from "mongoose";

export function connectDatabase() {
    mongoose.connect(
        "mongodb://localhost:27017/recipes",
        {
            useNewUrlParser: true,    
        }
    );

    const db = mongoose.connection;

    db.on("error", () => {
        console.log("Failed to connect to database");
    }).once("open", () => {
        console.log("Connected to mongodb");
    });

    return db;
}
