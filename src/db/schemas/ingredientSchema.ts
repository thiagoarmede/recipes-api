import {Schema} from 'mongoose';

export const IngredientSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    avatarUrl: {
        type: String,
        required: false
    }
});
