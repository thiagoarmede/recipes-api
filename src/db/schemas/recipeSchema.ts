import { Schema } from 'mongoose';
import { UserSchema } from './userSchema';
import { IngredientSchema } from './ingredientSchema';

export const RecipeSchema = new Schema({
    name: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: false,
    },
    cookTime: {
        type: Number,
        required: true,

    },
    cookDescription: {
        type: String,
        required: true,
    },
    author: {
        type: UserSchema,
        required: true,
    },
    ingredients: {
        type: [IngredientSchema],
        required: true,
    },
    stars: {
        type: Number,
        required: true,
    }
});