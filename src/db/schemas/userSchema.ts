import { Schema } from 'mongoose';

export const UserSchema = new Schema({
    name: {
        type: String,
        required: true,
    },
    age: {
        type: Number,
        required: false,
    },
    avatarUrl: {
        type: String,
        required: false,
    },
    nationality: {
        type: String,
        required: false,
    }
});