export const errorTap = (fn: (arg: any) => void) => (err: any) => {
    fn(err);
    throw err;
} 